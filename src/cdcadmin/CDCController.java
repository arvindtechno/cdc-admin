package cdcadmin;

import java.io.ByteArrayInputStream;
import test.Encryption;
import test.KeyGeneration;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import test.Main;

public class CDCController implements Initializable {
    private List<File> files;
    @FXML
    private Button choose;
    @FXML
    private Button set;
    @FXML
    private TextField sourcepath;
    @FXML
    private TextField destpath;
    @FXML
    private Button encrypt;
    @FXML
    private Button reset;
    @FXML
    private Button cancel;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    

    @FXML
    private void chooseAFile(ActionEvent event) {
        
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        files = fileChooser.showOpenMultipleDialog(null);
        if(files!=null){
            sourcepath.setText(files.toString());
        }
    }

    @FXML
    private void chooseAPlace(ActionEvent event) {
        DirectoryChooser fileChooser = new DirectoryChooser();
        fileChooser.setTitle("Open Resource File");
        File file = fileChooser.showDialog(null);
        if(file!=null){
            destpath.setText(file.getPath());
        }
    }

    @FXML
    private void encryptFile(ActionEvent event) throws FileNotFoundException, IOException, SQLException, ClassNotFoundException {
        if(files!=null && destpath!=null){
            FileInputStream fileInputStream;
            for(File f : files){
                fileInputStream = new FileInputStream(f);
                String file = destpath.getText()+"\\"+f.getName();

                if(f.getName().endsWith("html") || f.getName().endsWith("HTML")){
                    KeyGeneration keyGeneration = new KeyGeneration();
                    Final.Encryption encryption = new Final.Encryption();
                    encryption.encrypt(fileInputStream, file, keyGeneration.key);
                }else{
                    KeyGeneration keyGeneration = new KeyGeneration();
                    Encryption encryption = new Encryption();
                    String data = "012345678912345012345678912345";
                    byte[] newBs = data.getBytes();
                    ByteArrayInputStream fileInputStream1 = new ByteArrayInputStream(newBs);
                    byte[] buff = encryption.encrypt(fileInputStream1, keyGeneration.key);
                    try {
                        new Main(buff, f, file);
                    } catch (Exception ex) {
                        Logger.getLogger(CDCController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                Connection conn = null;
                try{
                    conn = DBConnect.getConnection();
                    Statement stmt = conn.createStatement();

                    String extention = file.substring(file.indexOf('.')+1);
                    file = file.replace("\\", "\\\\");
                    String query;
                    if(extention.equalsIgnoreCase("mp4")){
                        File source = f;
                        DurationClass durationClass = new DurationClass();
                        long size = source.length();
                        long duration = durationClass.getDuration(f.getAbsolutePath());
                        long datasize = (((size/duration)/1024)*10)+10;

                        query = "INSERT INTO fileaddress(`duration`, `size`, `dataSize`, `relativeAddress`, `absoluteAddress`, `topic_id`, `fileType`) VALUES (\'"+duration+"\', \'"+size+"\', \'"+datasize+"\', \""+file+"\",\'"+file+"\', '1000336', \'"+extention+"\')";
                    }else
                        query = "Insert into fileAddress(`relativeAddress`, `absoluteAddress`, `topic_id`, `fileType`) values(\""+file+"\",\'"+file+"\', '1000336', \'"+extention+"\')";

                    stmt.executeUpdate(query);
                }
                catch(SQLException e){
                    e.printStackTrace();
                }
            }
            sourcepath.setText("Data Encrypted");
            destpath.setText(null);
        }
    }

    @FXML
    private void Reset(ActionEvent event) {
        sourcepath.setText(null);
        destpath.setText(null);
    }

    @FXML
    private void Cancel(ActionEvent event) {
        Stage stage = (Stage) cancel.getScene().getWindow();
        stage.close();
    }
    
}
