/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cdcadmin;

import java.io.File;

/**
 *
 * @author Tiwari
 */
public class DurationClass {

    public long getDuration(String fileName){
        File file         = new File(fileName);
        MediaInfo info    = new MediaInfo();
        info.open(file);
        String duration = info.get(MediaInfo.StreamKind.Video, 0, "Duration", 
                                MediaInfo.InfoKind.Text, MediaInfo.InfoKind.Name);
        long durationI = Long.parseLong(duration)/1000;
        return durationI;
    }
    
    public static void main(String args[]){
        String fileName   = "C:\\Users\\Tiwari\\Desktop\\CDC DATA\\Brain_EN_QUIZ.swf";
        File file         = new File(fileName);
        MediaInfo info    = new MediaInfo();
        info.open(file);

        String format     = info.get(MediaInfo.StreamKind.Video, 0, "Format", 
                                MediaInfo.InfoKind.Text, MediaInfo.InfoKind.Name);
        String bitRate       = info.get(MediaInfo.StreamKind.Video, 0, "Duration", 
                                MediaInfo.InfoKind.Text, MediaInfo.InfoKind.Name);
        String frameRate   = info.get(MediaInfo.StreamKind.Video, 0, "FrameRate", 
                                MediaInfo.InfoKind.Text, MediaInfo.InfoKind.Name);
        String width       = info.get(MediaInfo.StreamKind.Video, 0, "Width", 
                                MediaInfo.InfoKind.Text, MediaInfo.InfoKind.Name);

        String audioBitrate  = info.get(MediaInfo.StreamKind.Audio, 0, "BitRate", 
                                MediaInfo.InfoKind.Text, MediaInfo.InfoKind.Name);
        String audioChannels = info.get(MediaInfo.StreamKind.Audio, 0, "Channels", 
                                MediaInfo.InfoKind.Text, MediaInfo.InfoKind.Name);
        
        System.out.println("Format : "+format);
        System.out.println("Format : "+bitRate);
        System.out.println("Format : "+frameRate);
        System.out.println("Format : "+width);
        System.out.println("Format : "+audioBitrate);
        System.out.println("Format : "+audioChannels);
    }
}
