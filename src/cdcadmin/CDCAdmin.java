
package cdcadmin;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class CDCAdmin extends Application {
    
    @Override
    public void start(Stage stage) throws IOException {
        AnchorPane root = (AnchorPane) FXMLLoader.load(getClass().getResource("admin.fxml"));

        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();

    }

    public static void main(String[] args) {
        Application.launch(CDCAdmin.class, (java.lang.String[])null);
    }
    
}
