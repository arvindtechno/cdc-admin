/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tiwari
 */
public class TestAESString {

    public TestAESString() {
        try {
            String data = "012345678912345012345678912345";
            byte[] newBs = data.getBytes();
            System.out.println("Data : "+(new String(newBs,0,newBs.length)));
            
            KeyGeneration keyGeneration = new KeyGeneration();
            Encryption encryption = new Encryption();
            Decryption decryption = new Decryption();
            
            ByteArrayInputStream fileInputStream = new ByteArrayInputStream(newBs);
            byte[] newBs1 = encryption.encrypt(fileInputStream, keyGeneration.key);
            
            System.out.println("Enc : "+(new String(newBs1,0,newBs1.length)));
            
//            try {
//                new Main(newBs1, "", "");
//            } catch (Exception ex) {
//                Logger.getLogger(TestAESString.class.getName()).log(Level.SEVERE, null, ex);
//            }

            InputStream in = new FileInputStream(new File("D:/btxt.txt"));
            in.read(newBs1);
            
            Main main = new Main();
            try {
                main.removeString(in, "");
            } catch (Exception ex) {
                Logger.getLogger(TestAESString.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(newBs1);
            
            byte[] newBs2 = decryption.decrypt(newBs1, arrayInputStream, keyGeneration.key);
            System.out.println("DATA = "+(new String(newBs2,0,newBs2.length)));
        } catch (IOException ex) {
            Logger.getLogger(TestAESString.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args){
        new TestAESString();
    }
    
}
