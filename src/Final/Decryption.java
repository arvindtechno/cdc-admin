/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Final;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Tiwari
 */
public class Decryption {

    public byte[][] Inv_S_Box = {
        {(byte)0x52, (byte)0x09, (byte)0x6A, (byte)0xD5, (byte)0x30, (byte)0x36, (byte)0xA5, (byte)0x38, (byte)0xBF, (byte)0x40, (byte)0xA3, (byte)0x9E, (byte)0x81, (byte)0xF3, (byte)0xD7, (byte)0xFB},
        {(byte)0x7C, (byte)0xE3, (byte)0x39, (byte)0x82, (byte)0x9B, (byte)0x2F, (byte)0xFF, (byte)0x87, (byte)0x34, (byte)0x8E, (byte)0x43, (byte)0x44, (byte)0xC4, (byte)0xDE, (byte)0xE9, (byte)0xCB},
        {(byte)0x54, (byte)0x7B, (byte)0x94, (byte)0x32, (byte)0xA6, (byte)0xC2, (byte)0x23, (byte)0x3D, (byte)0xEE, (byte)0x4C, (byte)0x95, (byte)0x0B, (byte)0x42, (byte)0xFA, (byte)0xC3, (byte)0x4E},
        {(byte)0x08, (byte)0x2E, (byte)0xA1, (byte)0x66, (byte)0x28, (byte)0xD9, (byte)0x24, (byte)0xB2, (byte)0x76, (byte)0x5B, (byte)0xA2, (byte)0x49, (byte)0x6D, (byte)0x8B, (byte)0xD1, (byte)0x25},
        {(byte)0x72, (byte)0xF8, (byte)0xF6, (byte)0x64, (byte)0x86, (byte)0x68, (byte)0x98, (byte)0x16, (byte)0xD4, (byte)0xA4, (byte)0x5C, (byte)0xCC, (byte)0x5D, (byte)0x65, (byte)0xB6, (byte)0x92},
        {(byte)0x6C, (byte)0x70, (byte)0x48, (byte)0x50, (byte)0xFD, (byte)0xED, (byte)0xB9, (byte)0xDA, (byte)0x5E, (byte)0x15, (byte)0x46, (byte)0x57, (byte)0xA7, (byte)0x8D, (byte)0x9D, (byte)0x84},
        {(byte)0x90, (byte)0xD8, (byte)0xAB, (byte)0x00, (byte)0x8C, (byte)0xBC, (byte)0xD3, (byte)0x0A, (byte)0xF7, (byte)0xE4, (byte)0x58, (byte)0x05, (byte)0xB8, (byte)0xB3, (byte)0x45, (byte)0x06},
        {(byte)0xD0, (byte)0x2C, (byte)0x1E, (byte)0x8F, (byte)0xCA, (byte)0x3F, (byte)0x0F, (byte)0x02, (byte)0xC1, (byte)0xAF, (byte)0xBD, (byte)0x03, (byte)0x01, (byte)0x13, (byte)0x8A, (byte)0x6B},
        {(byte)0x3A, (byte)0x91, (byte)0x11, (byte)0x41, (byte)0x4F, (byte)0x67, (byte)0xDC, (byte)0xEA, (byte)0x97, (byte)0xF2, (byte)0xCF, (byte)0xCE, (byte)0xF0, (byte)0xB4, (byte)0xE6, (byte)0x73},
        {(byte)0x96, (byte)0xAC, (byte)0x74, (byte)0x22, (byte)0xE7, (byte)0xAD, (byte)0x35, (byte)0x85, (byte)0xE2, (byte)0xF9, (byte)0x37, (byte)0xE8, (byte)0x1C, (byte)0x75, (byte)0xDF, (byte)0x6E},
        {(byte)0x47, (byte)0xF1, (byte)0x1A, (byte)0x71, (byte)0x1D, (byte)0x29, (byte)0xC5, (byte)0x89, (byte)0x6F, (byte)0xB7, (byte)0x62, (byte)0x0E, (byte)0xAA, (byte)0x18, (byte)0xBE, (byte)0x1B},
        {(byte)0xFC, (byte)0x56, (byte)0x3E, (byte)0x4B, (byte)0xC6, (byte)0xD2, (byte)0x79, (byte)0x20, (byte)0x9A, (byte)0xDB, (byte)0xC0, (byte)0xFE, (byte)0x78, (byte)0xCD, (byte)0x5A, (byte)0xF4},
        {(byte)0x1F, (byte)0xDD, (byte)0xA8, (byte)0x33, (byte)0x88, (byte)0x07, (byte)0xC7, (byte)0x31, (byte)0xB1, (byte)0x12, (byte)0x10, (byte)0x59, (byte)0x27, (byte)0x80, (byte)0xEC, (byte)0x5F},
        {(byte)0x60, (byte)0x51, (byte)0x7F, (byte)0xA9, (byte)0x19, (byte)0xB5, (byte)0x4A, (byte)0x0D, (byte)0x2D, (byte)0xE5, (byte)0x7A, (byte)0x9F, (byte)0x93, (byte)0xC9, (byte)0x9C, (byte)0xEF},
        {(byte)0xA0, (byte)0xE0, (byte)0x3B, (byte)0x4D, (byte)0xAE, (byte)0x2A, (byte)0xF5, (byte)0xB0, (byte)0xC8, (byte)0xEB, (byte)0xBB, (byte)0x3C, (byte)0x83, (byte)0x53, (byte)0x99, (byte)0x61},
        {(byte)0x17, (byte)0x2B, (byte)0x04, (byte)0x7E, (byte)0xBA, (byte)0x77, (byte)0xD6, (byte)0x26, (byte)0xE1, (byte)0x69, (byte)0x14, (byte)0x63, (byte)0x55, (byte)0x21, (byte)0x0C, (byte)0x7D}
    };
    
    private byte[][] Inv_Mix_Columns = {
        {0X0E, 0X0B, 0X0D, 0X09},
        {0X09, 0X0E, 0X0B, 0X0D},
        {0X0D, 0X09, 0X0E, 0X0B},
        {0X0B, 0X0D, 0X09, 0X0E}
    };
    
    byte[][] sKey = new byte[4][4];

    byte[][] data = new byte[4][4];
    
    public byte[][] decrypt(FileInputStream fileInputStream, String filePath, byte[][] key) throws IOException{
        
        File file = new File(filePath);
        if(file.exists())
            file.delete();
        file.createNewFile();
        
        FileOutputStream fileOutputStream1 = new FileOutputStream(file.getAbsolutePath());
        BufferedOutputStream stream = new BufferedOutputStream(fileOutputStream1);
        
        while(fileInputStream.available()!=0){
            dataCopy(fileInputStream);
            String dataString = "";
            squareKey(key, 56);
            addRoundKey(data, sKey);
            for(int i=1; i<=13; i++){
                data = invShiftRows(data);
                data = invSubBytes(data);
                squareKey(key, 56-i*4);
                data = addRoundKey(data, sKey);
                data = invMixColumns(data);
            }
            data = invShiftRows(data);
            data = invSubBytes(data);
            squareKey(key, 0);
            data = addRoundKey(data, sKey);

            for(int i=0;i<=3;i++)
                stream.write(data[i]);
        
            stream.flush();
        }
        return data;
    }
    
    public byte[][] shift3(byte[][] data){
        byte temp = data[0][3];
        data[0][3] = data[1][3];
        data[1][3] = data[2][3];
        data[2][3] = data[3][3];
        data[3][3] = temp;
        return data;
    }
    
    public byte[][] shift2(byte[][] data){
        byte temp = data[0][2];
        byte temp1 = data[1][2];
        data[0][2] = data[2][2];
        data[1][2] = data[3][2];
        data[2][2] = temp;
        data[3][2] = temp1;
        
        return data;
    }
    
    public byte[][] shift1(byte[][] data){
        byte temp = data[3][1];
        data[3][1] = data[2][1];
        data[2][1] = data[1][1];
        data[1][1] = data[0][1];
        data[0][1] = temp; 
        return data;
    }
    
    public byte[][] invSubBytes(byte[][] data){
        for(int i=0; i<=3; i++){
            for(int j=0; j<=3; j++){
                String hex = "0"+Integer.toHexString(data[j][i]);
                Integer len = hex.length();
                data[j][i] = Inv_S_Box[Integer.parseInt(hex.charAt(len-2)+"", 16)][Integer.parseInt(hex.charAt(len-1)+"", 16)];
            }
        }
        return data;
    }
    
    public byte[][] invShiftRows(byte[][] data){
        data = shift3(shift2(shift1(data)));
        return data;
    }
    
    public byte multiplyByTwo(byte data){
        data = (byte)(data << 1);
        if(check_High_Bit(data))
            data = (byte)(data ^ 0x1B);
        return data;
    }
    
    public Boolean check_High_Bit(byte mid){
        if((mid & 128)==128)
            return true;
        return false;
    }
    
    public byte[][] invMixColumns(byte[][] data){
        byte mid = 0;
        byte[][] tempData = new byte[4][4];
        for(int i=0; i<=3; i++){
            for(int j=0; j<=3; j++){
                for(int k=0; k<=3; k++){
                    switch(Inv_Mix_Columns[i][k]){
                        case 9 : 
                            mid = (byte)(multiplyByTwo(multiplyByTwo(multiplyByTwo(data[k][j]))) ^ data[k][j]);
                            break;
                        case 11 :
                            mid = (byte)((multiplyByTwo((byte)(multiplyByTwo(multiplyByTwo(data[k][j])) ^ data[k][j]))) ^ data[k][j]);
                            break;
                        case 13 :
                            mid = (byte)((multiplyByTwo(multiplyByTwo((byte)(multiplyByTwo(data[k][j]) ^ data[k][j])))) ^ data[k][j]);
                            break;
                        case 14 :
                            mid = ((multiplyByTwo((byte)(multiplyByTwo((byte)(multiplyByTwo(data[k][j]) ^ data[k][j])) ^ data[k][j]))));
                            break;
                    }
                    tempData[i][j] = (byte)(tempData[i][j]^mid);
                }
            }
        }
        return tempData;
    }
    
    public byte[][] addRoundKey(byte[][] data, byte[][] key){
        
        for(int i=0; i<=3; i++)
            for(int j=0; j<=3; j++)
                data[i][j] = (byte)(data[i][j] ^ key[i][j]);
        return data;
    }
    
    private void dataCopy(FileInputStream fileInputStream) throws IOException {
        for(int i = 0; i<4; i++){
            byte[] keyByte = new byte[4];
            fileInputStream.read(keyByte);
            data[i] = keyByte;    
        }
    }
    
    private byte[][] squareKey( byte[][] key, int i) {
        for(int j=0; j<=3; j++, i++){
            for(int k=0; k<=3; k++){
                sKey[k][j] = key [k][i];
            }
        }
        return null;
    }
    
}
